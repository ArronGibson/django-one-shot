from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.shortcuts import render
from django.db.models import Count
from .models import TodoList
from .forms import TodoListForm, TodoListUpdateForm


def todo_list_view(request):
    todo_lists = TodoList.objects.all().annotate(item_count=Count('items'))
    return render(request, 'todo/todo_list.html', {'todo_lists': todo_lists})


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    tasks = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "tasks": tasks,
    }
    return render(request, "todo/todo_list_detail.html", context)


class TodoListCreateView(CreateView):
    model = TodoList
    form_class = TodoListForm
    template_name = 'todo/todo_list_create.html'
    success_url = reverse_lazy('todo_list_list')


class TodoListUpdateView(UpdateView):
    model = TodoList
    form_class = TodoListUpdateForm
    template_name = 'todo/todo_list_update.html'
    success_url = reverse_lazy('todo_list_list')