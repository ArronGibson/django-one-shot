from django.urls import path
from . import views

urlpatterns = [
    path('', views.todo_list_view, name='todo_list_list'),
    path('create/', views.TodoListCreateView.as_view(), name='todo_list_create'),
    path('todo-list/<int:id>', views.todo_list_detail, name='todo_list_detail'),
    path('todo-list/<int:pk>/update', views.TodoListUpdateView.as_view(), name='todo_list_update'),
]